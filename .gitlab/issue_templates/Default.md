- **Foundry version & build:** e.g. v11.301
- **System version:** e.g. v1.1.0
- **Browser:** (If applicable)
- **Permission level:** Gamemaster / Player / Both

**Description**

Including reproduction steps, if applicable.
